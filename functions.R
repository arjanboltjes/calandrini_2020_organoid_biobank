#setwd("~/analyses/datasets/")
library(Seurat)
library(Matrix)
library(SCutils)
library(zeallot)
library(org.Hs.eg.db)
require(clusterProfiler)
require(IDPmisc)

#### * functions required for pre-processing

cbPalette <- c('#111111', "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7",  "#E6009F", "#00E656", "#EE5050", "#C86464", "#C8C8C8", "#999999")



color.palette <- function(steps, n.steps.between=NULL, ...){
  
  if(is.null(n.steps.between)) n.steps.between <- rep(0, (length(steps)-1))
  if(length(n.steps.between) != length(steps)-1) stop("Must have one less n.steps.between value than steps")
  
  fill.steps <- cumsum(rep(1, length(steps))+c(0,n.steps.between))
  RGB <- matrix(NA, nrow=3, ncol=fill.steps[length(fill.steps)])
  RGB[,fill.steps] <- col2rgb(steps)
  
  for(i in which(n.steps.between>0)){
    col.start=RGB[,fill.steps[i]]
    col.end=RGB[,fill.steps[i+1]]
    for(j in seq(3)){
      vals <- seq(col.start[j], col.end[j], length.out=n.steps.between[i]+2)[2:(2+n.steps.between[i]-1)]  
      RGB[j,(fill.steps[i]+1):(fill.steps[i+1]-1)] <- vals
    }
  }
  
  new.steps <- rgb(RGB[1,], RGB[2,], RGB[3,], maxColorValue = 255)
  pal <- colorRampPalette(new.steps, ...)
  return(pal)
}

steps.logscale2 <- c("grey90", "grey80", "grey60", '#4575b4','#74add1','#abd9e9','#fee090','#fdae61','#f46d43', '#d73027')
pal.logscale2 <- color.palette(steps.logscale2, c(15, 10, 10, 10, 10, 10, 10, 10, 15), space="rgb")

#return the provided matrix or dataframe without columns or rows with indicated names
remByName <- function(mat, margin, names){
  if (margin == 1){
    return(mat[!rownames(mat) %in% names,])
  }
  else if (margin == 2){
    return(mat[,!colnames(mat) %in% names])
  }
}


#given a matrix or dataframe and an optional pattern
#returns a list of two vectors: the ensembleIDs (what's before the pattern)
#and the genesymbols (what's after the pattern)
extractIDs <- function(dataTable, pattern = '__'){
  idList <- strsplit(rownames(dataTable), pattern)
  ensembleIDs <- lapply(idList, function(x)x[1])
  geneSymbols <- lapply(idList, function(x)x[2])
  
  ensembleIDs <- unlist(ensembleIDs)
  geneSymbols <- unlist (geneSymbols)
  
  return(list(ensembleIDs, geneSymbols))
}


#given ERCC and mitochondrial gene patters, return a list of 
#three tables, one only with rows corresponding to ERCC,
#one only with rows corresponding to mitochondrial
#one with rows corresponding to the rest of the genes
#the UNK well is removed from all resulting tables.
.tableSplit <- function(txpTable, erccPattern, mitoPattern ){
  #extract indices for ERCC, genes, mitocondrial genes
  txpTable <- remByName(txpTable,2,'UNK')
  erccIdx <- grep(erccPattern, rownames(txpTable))
  mitoIdx <- grep(mitoPattern, rownames(txpTable))
  
  #splitting table into mito, ercc, genes
  mitoTxpTable <- txpTable[mitoIdx, ]
  erccTxpTable <- txpTable[erccIdx, ]
  geneTxpTable <- txpTable[-c(mitoIdx, erccIdx), ]
  
  return(list(geneTxpTable, erccTxpTable, mitoTxpTable))
}


#??? i don't think this is needed at all. commenting it out in case it is actually needed somewhere
#.goodReaction<-function(){
#  return(list(geneTxpTable[,goodReactionWellNames], metadataTable[goodReactionWellNames,]))
#}


#given information about a sample, generate a metadata table containing information for every well in the plate
#can take optional dapiDim wells and mark them as positive.
#could be reimplemented to take an arbitrary number of elements by looping and adding to a list and do.call to create the final data.frame 
metadataGenerator<- function(metadata){

  factorNames <- colnames(metadata)
  factorList <- list()
  for (name in factorNames){
    factorList[[name]] <- rep(as.character(metadata[,name]), 385)
    names(factorList[[name]]) <- wellname()
  }
  
  generatedMetadata <- do.call(cbind, factorList) 
  as.data.frame(generatedMetadata)
  
  return(generatedMetadata)
  
}

#determine which wells need to be removed from a dataset by taking into account transcript number, empty wells, and failed reactions
#returns a cleaned up version of transcript table, metadata and ercc table
liveCells <- function(geneTxpTable, metadataTable, erccTxpTable, min.tpx=1000, ignore=character(0), empties=character(0)){
  #determine wells were reaction was good, and empty wells
  goodErcc <- goodERCCs(erccTxpTable, ignore = ignore, good.ERCC.median = 4, min.req.ERCCs = 2)
  goodReactionWellNames <- goodwells(goodErcc, ignore = ignore)
  if (length(empties) > 0){
    emptyWellNames <- empties
  }
  else{
    emptyWellNames <- c("A24","B24",'C24','D24','E24','F24','G24','H24','I24','J24','K24','L24','M24','N24','O24','P24')
  }
  
  wellsets <- list('all'=c(goodReactionWellNames,emptyWellNames), 'good'=goodReactionWellNames, 'empty'=emptyWellNames) 
  
  #determine the transcript cutoff automatically or set a hard cutoff
  cutoff <- livecellcutoff(genes = geneTxpTable, wellsets = wellsets)
  if (is.na(cutoff) || cutoff < min.tpx){
    cutoff <- min.tpx
  }
  
  #determine wells that need to be removed either because of bad reactions, or because empty
  badReactionWellNames = setdiff(wellname()[-1], goodReactionWellNames)
  wellsToRemove <- union(badReactionWellNames, emptyWellNames)
  cleanGeneTxpTable<- remByName(geneTxpTable,2,wellsToRemove)
  
  #determine wells that can be kept because they have > cutoff number of transcripts
  keepNames <- colnames(cleanGeneTxpTable)[colSums(cleanGeneTxpTable) >= cutoff]
  
  #return filtered list
  return(list(geneTxpTable[,keepNames], metadataTable[keepNames,], erccTxpTable[,keepNames]))
}

#take 
preProcPlate <- function(metadata, emptyWells, ignoredWells, inputFolder = "./", organism = "human", min.tpx =1000){
  #organism choice
  if (organism == "human") {
    mitoPattern <- "__MT-"
    erccPattern <- "^ERCC-"
  }
  
  if (organism == "mouse") {
    mitoPattern <- "__mt-"
    erccPattern <- "^ERCC-"
  }
  
  
  #read in transcripts file and prepare table
  tablePath <- paste(inputFolder, metadata$plateName, ".transcripts.txt", sep='')
  txpTable <- read.table(tablePath, header = TRUE, sep = "\t", as.is = TRUE, row.names=1, stringsAsFactors = FALSE)
  metadataTable <- metadataGenerator(metadata)
  
  #split the table in gene, mito and ercc components
  c(geneTxpTable, erccTxpTable, mitoTxpTable) %<-% .tableSplit(txpTable, erccPattern, mitoPattern)
  

  
  #live cells
  #liveCellFunctions here
  c(liveWellsTxpTable, liveWellsMetadataTable, liveWellsERCCTable) %<-% liveCells(geneTxpTable, metadataTable, erccTxpTable, 
                                                                                  min.tpx = min.tpx, empties=emptyWells, ignore = ignoredWells)
  

  plateName <- metadata$plateName
  colnames(liveWellsTxpTable) <- paste(plateName ,colnames(liveWellsTxpTable),sep='.')
  rownames(liveWellsMetadataTable) <- paste(plateName, rownames(liveWellsMetadataTable), sep='.')
  colnames(liveWellsERCCTable) <- paste(plateName, colnames(liveWellsERCCTable), sep='.')
  colnames(mitoTxpTable)  <- paste(plateName, colnames(mitoTxpTable), sep='.')
  mitoTxpTable <- mitoTxpTable[,colnames(liveWellsTxpTable)]
  return(list(liveWellsTxpTable, liveWellsMetadataTable, liveWellsERCCTable, mitoTxpTable))
}


#merge two dataframes by rownames and takes care of 1) setting the retained row.names as actual row.names and 2) deleting the column of previous row.names.
.mergeByRowNames <- function(dataFrame1, dataFrame2, all=TRUE){
  mergedDataFrame = merge(dataFrame1, dataFrame2, by ='row.names', all=all)
  rownames(mergedDataFrame) <- mergedDataFrame[,1]
  mergedDataFrame <- mergedDataFrame[,-1]
  return(mergedDataFrame)
}


#take a metametadata df with information about each sample, and a list of dapi coordinates for each sample to
#create the appropriate dataframe objects and determine live cells for every dataset. at the end, merges everything
# and creates 3 aggregate dataframes: expression matrix, ercc expression matrix, and metadata table for all cells.g
mergeDatasets <- function(metaMetaData, emptyWellsList, ignoredWellsList=list(), inputFolder= "./",  organism='human', min.tpx=1000){
  metadataList <- list()
  dataList <- list()
  erccList <- list()
  mitoList <- list()
  sampleNumber <- nrow(metaMetaData)
  
  if (!('plateName' %in% colnames(metaMetaData))){
    stop('A column named plateName needs to be present in the metadata')
  }
  
  for (i in 1:sampleNumber){
    sampleMetadata<- metaMetaData[i,]
    
    #establishing ignored wells
    if (length(ignoredWellsList) == 0){
      ignoreVar <- character(0)
    }else{
      ignoreVar <- ignoredWellsList[[as.character(sampleMetadata$plateName)]]
    }
    
    #e
    if (length(emptyWellsList) == 0){
      emptyVar <- character(0)
    }else{
      emptyVar <- emptyWellsList[[as.character(sampleMetadata$plateName)]]
    }
    
    c(data, metadata, erccs, mito) %<-% preProcPlate(sampleMetadata,
                                                     min.tpx = min.tpx,
                                                     emptyWells=emptyVar,
                                                     ignoredWells=ignoreVar)
    
    dataList[[i]] <- data
    metadataList[[i]] <- metadata
    erccList[[i]] <- erccs
    mitoList[[i]] <- mito
  }
  
  #merge
  if(sampleNumber == 1){
    mergedDataFrame <- dataList[[1]]
    mergedMetaDataFrame <- t(metadataList[[1]])
    mergedErccs <- erccList[[1]]
    mergedMito <- mitoList[[1]]
  }
  if (sampleNumber > 1){
    mergedDataFrame <- .mergeByRowNames(dataList[[1]], dataList[[2]])
    mergedMetaDataFrame <- .mergeByRowNames(t(metadataList[[1]]), t(metadataList[[2]]))
    mergedErccs <- .mergeByRowNames(erccList[[1]], erccList[[2]])
    mergedMito <- .mergeByRowNames(mitoList[[1]], mitoList[[2]])
  }
  if (sampleNumber > 2){
    for (i in 3:sampleNumber){ #first two are already merged so index starts at 3
      mergedDataFrame <- .mergeByRowNames(mergedDataFrame, dataList[[i]])
      mergedMetaDataFrame <- .mergeByRowNames(mergedMetaDataFrame, t(metadataList[[i]]))
      mergedErccs <- .mergeByRowNames(mergedErccs, erccList[[i]])
      mergedMito <- .mergeByRowNames(mergedMito, mitoList[[i]])
    }
  }



  mergedMito[is.na(mergedMito)] <- 0
  mergedDataFrame[is.na(mergedDataFrame)] <- 0
  mergedErccs[is.na(mergedErccs)] <- 0
  mergedMetaDataFrame <- t(mergedMetaDataFrame)
  
  return(list(mergedDataFrame, mergedMetaDataFrame, mergedErccs, mergedMito))
}

#### * 


#given a factor, calculate how many cells take each value of the factor
cellCount <- function(metaData, factorName){
  percentVector <- vector()
  absVector <- vector()
  
  for (factor in unique(metaData[,factorName])){
    factorPattern <- paste("^",factor, "$", sep='')
    factorAbsolute <- sum(metaData[,factorName] == factor) 
    factorPercent <- factorAbsolute / nrow(metaData)
    
    percentVector[factor] <- c(percentVector, factorPercent)
    absVector[factor] <- c(absVector, factorAbsolute)
  }
  
  return(list(absVector, percentVector))
}


#splits a list of gene names returning either symbols or ids
#essentially the same as extractIDs TODO: eliminate the extractIDs and use this funciton instead
splitNames <- function(names, return='symbols'){
  tempList <- strsplit(names,'--') #!!!
  if (return == 'symbols'){
    tempList <- lapply(tempList, function(x)x[2])
    return(unlist(tempList))
  }
  else if (return == 'ids'){
    tempList <- lapply(tempList, function(x)x[1])
    return(unlist(tempList))
  }
}


vecToColor <- function(x, colorpal= 'heat', outLength=15){
  library(LSD)
  #breaks <- seq(min(x), max(x), length.out=outLength)
  #groups <- cut(x, breaks=breaks, include.lowest=T)
  relativizedx <- (x - min(x)) / diff(range(x))
  colIndex <- floor(relativizedx * outLength +1) +1
  
  colors <- colorpalette(colorpal, nrcol= outLength+1)

  return(colors[colIndex])
}


GOCompare2 <- function(genesList, minPval=0.05, min.perc = 0.25, enrichWhat='GO', max.genes=500, splitGenes=TRUE, nCategories=50 ){
  require(clusterProfiler)
  library(AnnotationDbi)
  library(org.Hs.eg.db)
  
  #extract significant gene list for every cluster
  geneList <- list()
  geneList <- lapply(genesList, splitNames, 'ids')
  

  #set names
  if (!is.null(names(genesList))){
    names(geneList) <- names(genesList)
  }else{
    names(geneList) <- as.character(1:length(geneList))
  }
  
  
  
  #perform cluster finding
  if (enrichWhat == 'GO'){
    res.GO.BP <- compareCluster(geneList, fun="enrichGO", OrgDb='org.Hs.eg.db', ont= 'BP', keyType = 'ENSEMBL',
                                pAdjustMethod = "BH", pvalueCutoff  = 0.01, qvalueCutoff  = 0.05, readable= TRUE, minGSSize=15)
  }
  else if (enrichWhat == 'KEGG'){
    res.GO.BP <- compareCluster(geneList, fun="enrichKEGG", organism='hsa',, keyType = 'ENSEMBL',
                                keyType='ncbi-geneid', pAdjustMethod = "hochberg", pvalueCutoff  = 0.01, qvalueCutoff  = 0.05, readable= TRUE, minGSSize=15)
  }
  #res.GO.BP.drop <- dropGO(res.GO.BP, level=0:5)
  res.GO.BP.simpl <- simplify(res.GO.BP)
  clusterProfiler::dotplot(res.GO.BP.simpl, showCategory=nCategories)
  #return(res.GO.BP)
  return(res.GO.BP.simpl)
}


