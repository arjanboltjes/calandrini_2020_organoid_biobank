#load 3sept after filtering environment

allEnfObj <- readRDS("objects/allEnfObj.RDS")

#all early main figure
DimPlot(allEnfObj, reduction='tsne', cols=cbPalette ) + NoLegend()
DimPlot(allEnfObj, reduction='tsne', cols=cbPalette , group.by = 'organoid') + NoLegend()


markers<- c("EPCAM$", '-CDH1$', 'JAG1$', 'THY1$', 'COL1A1$', 'COL3A1$', 'NCAM1$', 'RET$', 'WNT6$')
markGood <- unlist(lapply(markers, grep, rownames(allEnfObj@assays$RNA@data), value=T))
p <-FeaturePlot(allEnfObj, features=markGood, reduction='tsne', cols = pal.logscale2(10), combine = FALSE, max.cutoff = 'q95')

for(i in 1:length(p)) {
  p[[i]] <- p[[i]] + NoLegend() 
}

cowplot::plot_grid(plotlist = p)

#early-late, sup figure
#EL51
EL51FObj <- readRDS("objects/EL51FObj.RDS")
DimPlot(EL51FObj, reduction='tsne', cols=cbPalette ) + NoLegend()
DimPlot(EL51FObj, reduction='tsne', cols=cbPalette[c(2,3)] , group.by = 'status') + NoLegend()

markers<- c("EPCAM$", '-CDH1$', 'JAG1$', 'THY1$', 'COL1A1$', 'COL3A1$', 'NCAM1$', 'RET$', 'WNT6$')
markGood <- unlist(lapply(markers, grep, rownames(EL51FObj@assays$RNA@data), value=T))
p <-FeaturePlot(EL51FObj, features=markGood, reduction='tsne', cols = pal.logscale2(10), combine = FALSE, max.cutoff = 'q95')

for(i in 1:length(p)) {
  p[[i]] <- p[[i]] + NoLegend() 
}

cowplot::plot_grid(plotlist = p)

#EL80
EL80FObj <- readRDS("objects/EL80FObj.RDS")

DimPlot(EL80FObj, reduction='tsne', cols=cbPalette ) + NoLegend()
DimPlot(EL80FObj, reduction='tsne', cols=cbPalette[c(2,3)] , group.by = 'status') + NoLegend()

#need to add zero vectors for some genes that aren't present:
testObj <- EL80FObj

aa <- rbind('ENSG00000154096--THY1'= rep(0,680),
            'ENSG00000165731--RET'= rep(0,680),
            "ENSG00000115596--WNT6" = rep(0,680))

testObj@assays$RNA@data <- rbind(testObj@assays$RNA@data,
                                 aa)

markers<- c("EPCAM$", '-CDH1$', 'JAG1$', 'THY1$', 'COL1A1$', 'COL3A1$', 'NCAM1$', 'RET$', 'WNT6$')
markGood <- unlist(lapply(markers, grep, rownames(testObj@assays$RNA@data), value=T))
p <-FeaturePlot(testObj, features=markGood, reduction='tsne', cols = pal.logscale2(10), combine = FALSE, max.cutoff = 'q95')

for(i in 1:length(p)) {
  p[[i]] <- p[[i]] + NoLegend() 
}

cowplot::plot_grid(plotlist = p)



#########################################
#####Differential expression#############
#########################################
originalOrgClusters <- list(E51=c(1,4,12,13),
                            E88=c(2,9,8,11),
                            E101=c(5,6,7,10),
                            E80=c(0,3))


testObj <- allEnfObj

origIdents <- Idents(testObj)
levels(origIdents) <- c("14" ,"1"  ,"12" ,"13" ,"6"  ,"5",  "2" , "7",  "10" ,"11" ,"8" ,"9","3", "4")

Idents(testObj) <- origIdents

DimPlot(testObj, reduction='tsne', cols=cbPalette, label=T )


allEnfObj <- testObj




correctedOrgClusters <- list(E51=c(1,2,3,4),
                             E88=c(9,10,11,12),
                             E101=c(5,6,7,8),
                             E80=c(13,14))


clusterDE <- function(sObj, orgClusters){
  cellNames <- colnames(sObj)
  projIdents <- Idents(sObj)
  
  Degenes <- list()
  
  for (organoid in names(orgClusters)){
    thisOrganoidClusters <- orgClusters[[organoid]]
    
    for (cluster in thisOrganoidClusters){
      baseIdent <- rep(20, length(cellNames))
      names(baseIdent) <- cellNames
      
      clustNames <- names(projIdents)[projIdents == cluster]
      
      restclusts <- thisOrganoidClusters[!thisOrganoidClusters == cluster]
      restNames <- names(projIdents)[projIdents %in% restclusts]
      
      
      
      baseIdent[clustNames] <- rep(0, length(clustNames))
      baseIdent[restNames] <- rep(1, length(restNames))
      
      sObj <- SetIdent(sObj, value= baseIdent)
      
      marks <- FindMarkers(sObj, ident.1=0, ident.2=1, only.pos=T, logfc.threshold = 0.59)
      marks <- marks[marks$p_val_adj < 0.05,]
      marks <- marks[order(-marks$avg_logFC),]
      
      
      Degenes[[as.character(cluster)]] <- marks
      
    }
    
  }
  return(Degenes)
}

clustDEGenes <- clusterDE(allEnfObj, correctedOrgClusters)


#ge organoid specific stuff


organoidDE <- function(sObj, orgClusters){
  cellNames <- colnames(sObj)
  projIdents <- Idents(sObj)
  
  Degenes <- list()
  
  for (organoid in names(orgClusters)){
    thisOrganoidClusters <- orgClusters[[organoid]]
    
    baseIdent <- rep(1, length(cellNames))
    names(baseIdent) <- cellNames
    
    clustNames <- names(projIdents)[projIdents %in% thisOrganoidClusters]
    
    baseIdent[clustNames] <- rep(0, length(clustNames))
    
    sObj <- SetIdent(sObj, value= baseIdent)
    
    marks <- FindMarkers(sObj, ident.1=0, ident.2=1, only.pos=T, logfc.threshold = 0.59)
    marks <- marks[marks$p_val_adj < 0.05,]
    marks <- marks[order(-marks$avg_logFC),]
    
    
    Degenes[[as.character(organoid)]] <- marks
    
    
    
  }
  return(Degenes)
}

OrganoidDEGenes <- organoidDE(allEnfObj, correctedOrgClusters)

#merging
mergedOrgClustDE <- c(OrganoidDEGenes, clustDEGenes)
a <- c("E51", '1','2','3','4','E101', '5', '6', '7','8', 'E88', '9', '10', '11', '12', 'E80', '13', '14')
mergedOrgClustDE <- mergedOrgClustDE[a]
#write everything out to excel
library(openxlsx)
testList <- mergedOrgClustDE
listNames <- names(testList)
OUT <- createWorkbook()
for (i in listNames){
  rownames(testList[[i]]) <- splitNames(rownames(testList[[i]]))
  addWorksheet(OUT, i)
  openxlsx::writeData(OUT, x=testList[[i]], sheet=i,rowNames = TRUE)
}
saveWorkbook(OUT, "test.xlsx", overwrite=T)



#GO analysis
DENames <- lapply(mergedOrgClustDE, rownames)
a <- c("E51", '1','2','3','4','E101', '5', '6', '7','8', 'E88', '9', '10', '11', '12', 'E80', '13', '14')
testList <- DENames[a]
GOProfile <- GOCompare2(testList)
clusterProfiler::dotplot(GOProfile)



#% early late in clusters
#cluster %
#EL51
DimPlot(EL51FObj, reduction='tsne', label=T)
testoObj <- EL51FObj
a <- c('3', '2', '4', '5', '1','7', '6')
currIdents <- Idents(testoObj)
levels(currIdents) <- a
Idents(testoObj) <- currIdents
DimPlot(testoObj, reduction='tsne', label=T)

anno2 <- testoObj@meta.data[, "status"]
anno1 <- as.character(Idents(testoObj))
factor_table <- prop.table(x = table(anno1, anno2), margin = 2)
colnames(factor_table) <- new.cluster_ids

factor_table *100


#EL80
DimPlot(EL80FObj, reduction='tsne', label=T)
testoObj <- EL80FObj
a <- c('3', '5', '4', '2', '1','6')
currIdents <- Idents(testoObj)
levels(currIdents) <- a
Idents(testoObj) <- currIdents
DimPlot(testoObj, reduction='tsne', label=T)

anno2 <- testoObj@meta.data[, "status"]
anno1 <- as.character(Idents(testoObj))
factor_table <- prop.table(x = table(anno1, anno2), margin = 2)
colnames(factor_table) <- new.cluster_ids

factor_table *100

##additional featureplots
markers<- c('-IGF2$', '-SIX1$', '-SIX2$', '-CITED1$', '-H19$', '-WT1$')
markGood <- unlist(lapply(markers, grep, rownames(allEnfObj@assays$RNA@data), value=T))
p <-FeaturePlot(allEnfObj, features=markGood, reduction='tsne', cols = pal.logscale2(10), combine = FALSE, max.cutoff = 'q95')

for(i in 1:length(p)) {
  p[[i]] <- p[[i]] + NoLegend() 
}

cowplot::plot_grid(plotlist = p)
